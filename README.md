# ofxMarchingCubes

C++ library for OpenFrameworks implementing the Marching Cubes algorithms based on [this article](http://paulbourke.net/geometry/polygonise/) by Paul Bourke 
The marching cubes is an algorithm to create volumetric 3d shapes (aka Meta Balls!)

Check this [video](https://www.youtube.com/watch?v=2WKsddJ7-BQ) for an example 

## Getting started

- Download OpenFrameworks from www.openframeworks.cc
- Create a new project
- Copy the src folder to the addons folder of your project and add it to the header search path of your project
- *OPTIONAL* Replace the files in your project with the files in the example folder if you want to explore the provided example

## License
MIT license - https://opensource.org/licenses/MIT

## Roadmap
This library has not been updated in a while and will only be updated in case I require to use it on a new project (sorry..)

## Author
Rui Madeira